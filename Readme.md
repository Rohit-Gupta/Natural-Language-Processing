# **AUTONOMOUS SYSTEM USING NATURAL LANGUAGE PROCESSING** #

## PREFACE
>The objective of the project was “to understand Human Computer Interaction” and “to design and implement a system which takes natural language as input, processes it according to a defined business logic, and properly respond with a natural output”. For that we have to understand what Natural Language Processing is? , how is Natural Language processing related to Human Computer Interaction? , what is an Autonomous System? , how can we contribute to it? , and search for a defined business logic to implement.

>**Natural language processing (NLP)** is a field of computer science, artificial intelligence and computational linguistics concerned with the interactions between computers and human (natural) languages, and, in particular, concerned with programming computers to fruitfully process large natural language corpora. Challenges in natural language processing frequently involve natural language understanding, natural language generation (frequently from formal, machine-readable logical forms), connecting language and machine perception, dialog systems, or some combination thereof.

>**HCI (human-computer interaction)** is the study of how people interact with computers and to what extent computers are or are not developed for successful interaction with human beings. A significant number of major corporations and academic institutions now study HCI. Historically and with some exceptions, computer system developers have not paid much attention to computer ease-of-use. Many computer users today would argue that computer makers are still not paying enough attention to making their products "user-friendly." However, computer system developers might argue that computers are extremely complex products to design and make and that the demand for the services that computers can provide has always outdriven the demand for ease-of-use.

>**Autonomous System** is a type of a system that is cable of performing and making decisions of its own based on a pre-defined logic. An autonomous system can be an embedded or a pure software or a pure hardware system.


## PREFACE

>The project was started on 25th May 2017 under the kind guidance of Dr Sanjeev Singh, HoD, Institute of Informatics and Communication. The first part of the project involved learning all the perquisites like Python, NLTK, Natural Language Processing, etc. 


## INTRODUCTION

>The objective of the project was “to understand Human Computer Interaction” and “to design and implement a system which takes natural language as input, processes it according to a defined business logic, and properly respond with a natural output”. For that we have to understand what Natural Language Processing is? , how is Natural Language processing related to Human Computer Interaction? , what is an Autonomous System? , how can we contribute to it? , and search for a defined business logic to implement.

>**Natural language processing (NLP)** is a field of computer science, artificial intelligence and computational linguistics concerned with the interactions between computers and human (natural) languages, and, in particular, concerned with programming computers to fruitfully process large natural language corpora. Challenges in natural language processing frequently involve natural language understanding, natural language generation (frequently from formal, machine-readable logical forms), connecting language and machine perception, dialog systems, or some combination thereof.


>**HCI (human-computer interaction)** is the study of how people interact with computers and to what extent computers are or are not developed for successful interaction with human beings. A significant number of major corporations and academic institutions now study HCI. Historically and with some exceptions, computer system developers have not paid much attention to computer ease-of-use. Many computer users today would argue that computer makers are still not paying enough attention to making their products "user-friendly." However, computer system developers might argue that computers are extremely complex products to design and make and that the demand for the services that computers can provide has always outdriven the demand for ease-of-use.


>**Autonomous System** is a type of a system that is cable of performing and making decisions of its own based on a pre-defined logic. An autonomous system can be an embedded or a pure software or a pure hardware system.

## **Things studied During the projects**
> 
#### 1. Weka
>    Weka or Walkato Enviornment for Knowledge Analysis is a suite of machine learning software written in Java. It is free software licensed under the [GNU General Public License](en.wikipedia.org/wiki/GNU_General_Public_License). Weka contains a collections of visualization tools and algorithm for data analysis and predictive modeling, together with graphical user interface for easy access to these functions. Weka's main user interface is the *Expolorer*, but essentially the same functionality can be accessed through the component-based *Knowledge Flow* interface and from the command line. There is also the *Experimenter*, which allows the systematic comparison of the predictive performance of Weka's machine learning algorithms on a collection of datsets.
#### 2. IBM Watson BlueMix
>    IBM Bluemix is a cloud platform as a service (PaaS) developed by IBM. It supports several programming languages and services as well as integrated DevOps to build, run, deploy and manage applications on the cloud. Bluemix is based on Cloud Foundry (a Multi Cloud apllication ) open technology and runs on SoftLayer (a dedicated server, managed hosting and cloud computing provider) infrastructure.
#### 3. Porter2 Stemming Algorithm
>    Porter's algorithm consists of 5 phrases of word reduction, applied sequentially. Within each phrase there are various conventions to select rules, such as selecting rule from each group that applies to the longest suffix. In the first phase, this convention is used within the following rule group:
    *Rule*:                 *Example*
    SSES --->  SS           caresses  --->  caress
    IES  --->  I            ponies    --->  poni
    SS   --->  SS           caress    --->  caress
    S    --->               cats      --->  cats
#### 4. PocketSphinx
>    A version of Sphinx that can be used in embedded systems (e.g., based on an ARM processor). PocketSphinx is under active development and incorporates features such as fixed-point arithmetic and efficient algorithms for GMM computation.
#### 5. CMUSphinx Knowledge Base
>    The lmtool builds a consistent set of lexical and language model files for decoders. The target decoders are the Sphinx family, though any system that can read ARPA-format files can use them.
    Currently lmtool is configured for the English language (and its American dialect in particular). If you upload a corpus in a different language, your output is likely unpredictable. We are working on this. The current version does not deal gracefully with Unicode; this is also being worked on. 
#### 6. SphinxTrain
>    It is a acostic model that is used along with CMU Sphinx. An acoustic model is used in Automatic Speech Recognition to represent the relationship between an audio signal and the phonemes or other linguistic units that make up speech. The model is learned from a set of audio recordings and their corresponding transcripts. It is created by taking audio recordings of speech, and their text transcriptions, and using software to create statistical representations of the sounds that make up each word.
#### 7. Sphinx Base
>    It is the CMU open source library that is used by the CMU Sphinx to run
#### 8. Cmusphinx-code
>    It is the CMU open source library that is used by the CMU Sphinx.
#### 9. Gitlab
>    It is a web-based Git ( a version control system for tracking changes in computer files and coordinoates work on those files among multiple people, primarily used for source code management in software development) Repository manager with wiki and issue tracking faetures, using an open source license, developed by GitLab Inc.
#### 10. Machine Learning Basics
>    Machine Learning is a method of data analysis that automates anlytical model building. Using algorithms that iteratively learn from data, machine learning allows computers to find hidden insights without being explicitly programmed where to look
#### 11. Python
>    Python is a widely used high-level programming language for general purpose programming, it is an interprated language. Python is a multi-paradigm-programming: OOP and structured programming are fully supported
#### 12. MarkDown
>    MarkDown is a lightweight markup language ( a markup language is a system for annotating a documnet ina way that is syntactically distinguishable from the text) with plain text formatting syntax. It is designed so that it can be converted to HTML and many other formats using a tool by the same name.
#### 13. NLTK (Natural Language Processing Toolkit)
>    It is a NLP based toolkit that is designed so as to faciliate manipilation of Naturally Processed data.
#### 14. TensorFlow
>    TensorFlow is an open-source software library for machine learning across a range of tasks, and developed by Google to meet their needs for systems capable of building and training neural networks to detect and decipher patterns and correlations, analogous to the learning and reasoning which humans use
#### 15. eSpeak
>   eSpeakNG is a compact open source software speech synthesizer for Linux, Windows and other platforms. It uses a formant synthesis method, providing many languages in a small size. Much of the programming for eSpeakNG's language support is done using rule files with feedback from native speakers.

>The project was started on 25th May 2017 under the kind guidance of Dr Sanjeev Singh, HoD, Institute of Informatics and Communication. The first part of the project involved learning all the perquisites like Python, NLTK, Natural Language Processing, etc. 


## **Things studied During the projects**
> 
#### 1. Weka
>    Weka or Walkato Enviornment for Knowledge Analysis is a suite of machine learning software written in Java. It is free software licensed under the [GNU General Public License](en.wikipedia.org/wiki/GNU_General_Public_License). Weka contains a collections of visualization tools and algorithm for data analysis and predictive modeling, together with graphical user interface for easy access to these functions. Weka's main user interface is the *Expolorer*, but essentially the same functionality can be accessed through the component-based *Knowledge Flow* interface and from the command line. There is also the *Experimenter*, which allows the systematic comparison of the predictive performance of Weka's machine learning algorithms on a collection of datsets.
#### 2. IBM Watson BlueMix
>    IBM Bluemix is a cloud platform as a service (PaaS) developed by IBM. It supports several programming languages and services as well as integrated DevOps to build, run, deploy and manage applications on the cloud. Bluemix is based on Cloud Foundry (a Multi Cloud apllication ) open technology and runs on SoftLayer (a dedicated server, managed hosting and cloud computing provider) infrastructure.
#### 3. Porter2 Stemming Algorithm
>    Porter's algorithm consists of 5 phrases of word reduction, applied sequentially. Within each phrase there are various conventions to select rules, such as selecting rule from each group that applies to the longest suffix. In the first phase, this convention is used within the following rule group:
    *Rule*:                 *Example*
    SSES --->  SS           caresses  --->  caress
    IES  --->  I            ponies    --->  poni
    SS   --->  SS           caress    --->  caress
    S    --->               cats      --->  cats
#### 4. PocketSphinx
>    A version of Sphinx that can be used in embedded systems (e.g., based on an ARM processor). PocketSphinx is under active development and incorporates features such as fixed-point arithmetic and efficient algorithms for GMM computation.
#### 5. CMUSphinx Knowledge Base
>    The lmtool builds a consistent set of lexical and language model files for decoders. The target decoders are the Sphinx family, though any system that can read ARPA-format files can use them.
    Currently lmtool is configured for the English language (and its American dialect in particular). If you upload a corpus in a different language, your output is likely unpredictable. We are working on this. The current version does not deal gracefully with Unicode; this is also being worked on. 
#### 6. SphinxTrain
>    It is a acostic model that is used along with CMU Sphinx. An acoustic model is used in Automatic Speech Recognition to represent the relationship between an audio signal and the phonemes or other linguistic units that make up speech. The model is learned from a set of audio recordings and their corresponding transcripts. It is created by taking audio recordings of speech, and their text transcriptions, and using software to create statistical representations of the sounds that make up each word.
#### 7. Sphinx Base
>    It is the CMU open source library that is used by the CMU Sphinx to run
#### 8. Cmusphinx-code
>    It is the CMU open source library that is used by the CMU Sphinx.
#### 9. Gitlab
>    It is a web-based Git ( a version control system for tracking changes in computer files and coordinoates work on those files among multiple people, primarily used for source code management in software development) Repository manager with wiki and issue tracking faetures, using an open source license, developed by GitLab Inc.
#### 10. Machine Learning Basics
>    Machine Learning is a method of data analysis that automates anlytical model building. Using algorithms that iteratively learn from data, machine learning allows computers to find hidden insights without being explicitly programmed where to look
#### 11. Python
>    Python is a widely used high-level programming language for general purpose programming, it is an interprated language. Python is a multi-paradigm-programming: OOP and structured programming are fully supported
#### 12. MarkDown
>    MarkDown is a lightweight markup language ( a markup language is a system for annotating a documnet ina way that is syntactically distinguishable from the text) with plain text formatting syntax. It is designed so that it can be converted to HTML and many other formats using a tool by the same name.
#### 13. NLTK (Natural Language Processing Toolkit)
>    It is a NLP based toolkit that is designed so as to faciliate manipilation of Naturally Processed data.
#### 14. TensorFlow
>    TensorFlow is an open-source software library for machine learning across a range of tasks, and developed by Google to meet their needs for systems capable of building and training neural networks to detect and decipher patterns and correlations, analogous to the learning and reasoning which humans use
#### 15. eSpeak
>   eSpeakNG is a compact open source software speech synthesizer for Linux, Windows and other platforms. It uses a formant synthesis method, providing many languages in a small size. Much of the programming for eSpeakNG's language support is done using rule files with feedback from native speakers.

## **Procedure followed to create the project**

>Following all this we moved on to making a prototype of for the project, during which we made **a python program that would open/close the door based on the given command**.

>The efficiency of the system when exposed to repeated testing resulted into a low graph of positive responses. So we introduced the concept of **Stemming and Limitation**, i.e. now our prototype was able to process any of the variations of the word open and close like closed or closing. Which resulted into an increase in the number of positive results.

>The stemming algorithm that was used is **Porter2 Stemming and Lemitation algorithm**, which was found to be most efficient with words that were lexically hard to decode.

### to import stemmer
```
    firefox <https://pypi.python.org/pypi/stemming/1.0>
    DOWNLOAD THE stemming-1.0.tar.gz (md5)
    mv /home/downloads/[package] /home/speech_recognition/
    tar -xvfz [package]
    sudo python setup.py install
```

>Then to take the prototype a level up we incorporated the concept in which we were able to close and well as open the door at the same time.
After succeeding to be able to create a logic not of a kind of a business logic, we moved on the concepts of **Natural language Processing, i.e. NLP understanding and generation.**

>To achieve Natural Language Generation we started working on CMU Sphinx, an open source speech recognition system for mobile and server applications. In the starting we chose a test case of generating a knowledge base of 10 words and based on theses 10 words we tested the speech recognition system.

>At the earlier stages the system was not performing as to the expectations so we chose to go for the **Java based CMU Sphinx i.e. Sphinx4**, but we found the **python based CMU Sphinx i.e. Pocket Sphinx** to be performing better than the others.

>Then we generated the Knowledge base by taking a dictionary of 500 words, the efficiency of the CMU Sphinx reduced, but we were able to achieve an efficiency that was sufficient for our prototype to work well.

>In the whole process of the CMU Sphinx speech recognition we used:
1. Pocket Sphinx 
2. Sphinx Train
3. Sphinx Base
4. CMU Sphinx-code
5. CMU Sphinx Knowledge Base
6. Sphinx4

```
sudo su
sudo apt install svn
sudo apt-get install espeak
```

### to create the speech_recognition folder
```
sudo apt-get install gcc automake autoconf libtool bison swig python-dev libpulse-dev
mkdir speech_recognition
cd speech_recognition
```

### to download sphinxbase
```
git clone <https://github.com/cmusphinx/sphinxbase.git>
cd sphinxbase
./autogen.sh
make clean all
make check
sudo make install
```

### to configure the path
```
export LD_LIBRARY_PATH=/usr/local/lib
sudo nano /etc/ld.so.conf
WRITE THE FOLOLOWING TO THE OPENED FILE (IGNORE IF ALREADY THERE)
/usr/local/lib
sudo ldconfig          //to apply the configuration
```

### to download pocketsphinx
```
git clone <https://github.com/cmusphinx/pocketsphinx.git>
cd pocketsphinx
./autogen.sh
make clean all
make check
sudo make install
```

### to download sphinxtrain
```
git clone <https://github.com/cmusphinx/sphinxtrain.git>
cd sphinxtrain
./autogen.sh
make clean all
make check
sudo make install
```

### to download cmusphinx-code
```
svn checkout svn://svn.code.sf.net/p/cmusphinx/code/trunk cmusphinx-code
cd cmusphinx-code 
cd cmuclmtk
make clean all
make check
sudo make install
```

>Then once we have achieved Speech-to-text we integrated it with our prototype, the next objective that we needed to achieve to be able to fulfil the statement saying, “properly respond with a natural output”, was to be able to give output in speech format. Hence we used **eSpesak**, to achieve this.

>And when we tested the whole prototype we were satisfied with the outcomes.

>Once we were able to make a prototype, we thought of incorporating a bit of Machine Learning, so we when on to study **Tensorflow**. We got to know about how we can use Tensorflow to make a chat bot. That would respond to each of the questions asked. We even got to know about JEASA.

### to generate the knowledge base
```
cd ..
gedit corpus.txt
TYPE IN ALL THE WORD OR COMMANDS THAT YOU WANT TO MAKE THE SYSTEM RECOGNISE
firefox <http://www.speech.cs.cmu.edu/tools/lmtool-new.html>
CLICK ON THE BROWSE BUTTON AND SELECT THE CORPUS.TXT
CLICK THE COMPILE KNOWLEDGE BASE BUTTON
DOWNLOAD THE TAR FILE
mv /home/Downlaods/ /home/speech_recognition
tar -xvf [TAR FILE]
```

### to take the speech input
```
sudo pocketsphinx_continuous -inmic yes -lm 5371.lm -dict 5371.dic > NLP.txt
```

### to create the program
```
vim changeDoorState.py
```

### program
``` python
from stemmers import porter2            #importing poter2 stemming nad lemmitition algorithm
from espeak import espeak               #importing eapeak to facilitate text to speech
import time                             #importing time to facilitate time gap

def change_state(door_state, text):
	
	if isValid(text) == False:         #if text is not valid then show the current door_state 
		return door_state

	open_key = ['open','door']
	close_key = ['close','door']
		
	stem_cmd = [porter2.stem(i) for i in text]  #

	if door_state == False:            #if door_state is false the open the door
		key = open_key
	elif door_state == True:            #if door_state is true the close the door
		key = close_key
	
	for w in key:
		if w not in stem_cmd:
			print("invalid command")    #if the input is other than what is defined then print and speak "inalid command"
			espeak.set_voice("ru")
			espeak.synth("invalid command")
			time.sleep(1)
			return door_state
	
	door_state = not door_state

	if door_state == True:              #opens the door
		print("Door Opened")
		espeak.set_voice("ru")
		espeak.synth("Door Opened")
		time.sleep(1)
	else:                               #closes the door
		print("Door Closed")
		espeak.set_voice("ru")
		espeak.synth("Door Closed")
		time.sleep(1)
	
	return door_state
 	
# Checks whether the natural language input is valid or not.
def isValid(text):                              
	stemmed = [porter2.stem(i) for i in text]
                                                        #applying the porter2 algorithm
	if (len(text) == 1) or ('open' in stemmed and 'close' in stemmed):
		print("Invalid command.")
		espeak.set_voice("ru")
		espeak.synth("Invalid Command")
		time.sleep(1)
		return False

	return True
```

### to create the program
```
vim opendoor.py
```
### Program
``` python
from changeDoorState import change_state    #importing the changeDoorstate module
from espeak import espeak                   #importing espeak to facilitate text to speech
import time                                 #importing time to give the time delay

door_state = False                          #door is closed
                                            # True => door is opened
                                            # False => door is closed
f=open("NLP.txt","r+")                      #Opening the NLP.txt file
while True: 

	if door_state == True:                  #if Door_state is true the print and speak door opened
		print("Door state: Opened")
		espeak.set_voice("ru")
		espeak.synth("Door state: Opened")
		time.sleep(1)		
	else:                                   #if Door_state is true the print and speak door closed
		print("Door state: Closed")
		espeak.set_voice("ru")
		espeak.synth("Door state: Closed")
		time.sleep(1)
	
	cmd=f.readline()                        #read a line from the NLP.txt
	command = cmd.split()                   #split each line into separte words
	if len(command) == 0:
		continue
	door_state = change_state(door_state, command)      
```

### the to get output
```
python opendoor.py
```

## OUTPUT

![PocketSphinx_Continous](pocketsphinx_continous.png)

![Output of PocketSphinx](OUTPUT.png)

![Final Output](OUtput_final.png)

## CONCLUSION
>At the end, we successfully build a system that satisfies the given problem statement i.e. to design and implement a system which takes natural language as input, processes it according to a defined business logic, and properly respond with a natural output.
>The system so designed is an Autonomous Door opening and closing system that takes Natural Language input i.e. speech, processes it a according to the defined logic i.e. if commended to open the door it opens the door and if commanded to close the door it closes the door, if any weird or wage command is given it detects its and responds accordingly, And properly responding with a Natural Output i.e. Speech.

