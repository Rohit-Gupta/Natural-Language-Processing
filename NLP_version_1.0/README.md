# Procedure to create the project
```
sudo su
sudo apt install svn
sudo apt-get install espeak
```

## to create the speech_recognition folder
```
Sudo apt-get install gcc automake autoconf libtool bison swig python-dev libpulse-dev
mkdir speech_recognition
cd speech_recognition
```

## to download sphinxbase
```
git clone https://github.com/cmusphinx/sphinxbase.git
cd sphinxbase
./autogen.sh
make clean all
make check
sudo make install
```

## to configure the path
```
export LD_LIBRARY_PATH=/usr/local/lib
sudo nano /etc/ld.so.conf
WRITE THE FOLOLOWING TO THE OPENED FILE (IGNORE IF ALREADY THERE)
/usr/local/lib
sudo ldconfig          //to apply the configuration
```

## to download pocketsphinx
```
git clone https://github.com/cmusphinx/pocketsphinx.git
cd pocketsphinx
./autogen.sh
make clean all
make check
sudo make install
```

## to download sphinxtrain
```
git clone https://github.com/cmusphinx/sphinxtrain.git
cd sphinxtrain
./autogen.sh
make clean all
make check
sudo make install
```

## to download cmusphinx-code
```
svn checkout svn://svn.code.sf.net/p/cmusphinx/code/trunk cmusphinx-code
cd cmusphinx-code 
cd cmuclmtk
make clean all
make check
sudo make install
```

## to generate the knowledge base
```
cd ..
gedit corpus.txt
TYPE IN ALL THE WORD OR COMMANDS THAT YOU WANT TO MAKE THE SYSTEM RECOGNISE
firefox http://www.speech.cs.cmu.edu/tools/lmtool-new.html
CLICK ON THE BROWSE BUTTON AND SELECT THE CORPUS.TXT
CLICK THE COMPILE KNOWLEDGE BASE BUTTON
DOWNLOAD THE TAR FILE
mv /home/Downlaods/ /home/speech_recognition
tar -xvf [TAR FILE]
```

## to take the speech input
```
sudo pocketsphinx_continuous -inmic yes -lm 5371.lm -dict 5371.dic > NLP.txt
```

## to create the program
```
gedit changeDoorState.py
```

## program
``` python
from stemmers import porter2
from espeak import espeak

door_state = 1 # door is closed

def change_state(door_state, text):
	if isValid(text) == False:
		return
	open_key = ['open','door']
	close_key = ['close','door']
	stem_cmd = [porter2.stem(i) for i in text]
	if door_state == 0:
		key = open_key
	elif door_state == 1:
		key = close_key
	count = 0
	for w in key:
		if w in stem_cmd:
			count = count + 1		
	if count == len(key):
		if door_state == 0:
			door_state = 1
		elif door_state == 1:
			door_state = 0
	if door_state == 1:
		print("Door Opened")
		espeak.set_voice("ru")
		espeak.synth("Door Opened")
		while espeak.is_playing:
			pass
	else:
		print("Door Closed")
		espeak.set_voice("ru")
		espeak.synth("Door Closed")
		while espeak.is_playing:
			pass

## Checks whether the natural language input is valid or not.
def isValid(text):
	stemmed = [porter2.stem(i) for i in text]
	if (len(text) == 1) or ('open' in stemmed and 'close' in stemmed):
		print("Invalid command.")
		return False
	return True

f=open("NLP.txt","r+")
cmd=f.read(20)
command = cmd.split()
change_state(door_state, command)
```
# to import stemmer
```
    firefox https://pypi.python.org/pypi/stemming/1.0
    DOWNLOAD THE stemming-1.0.tar.gz (md5)
    mv /home/downloads/[package] /home/speech_recognition/
    tar -xvfz [package]
    sudo python setup.py install
```

## the to get output
```
python changeDoorState.py
```
