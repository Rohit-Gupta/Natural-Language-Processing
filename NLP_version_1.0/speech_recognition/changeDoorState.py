from stemmers import porter2
from espeak import espeak
import time

def change_state(door_state, text):
	
	if isValid(text) == False:
		return door_state

	open_key = ['open', 'door']
	close_key = ['close', 'door']
		
	stem_cmd = [porter2.stem(i) for i in text]

	if door_state == False:
		key = open_key
	elif door_state == True:
		key = close_key
	
	for w in key:
		if w not in stem_cmd:
			print("invalid command")
			espeak.set_voice("ru")
			espeak.synth("invalid command")
			time.sleep(1)
			return door_state
	
	door_state = not door_state

	if door_state == True:
		print("Door Opened")
		espeak.set_voice("ru")
		espeak.synth("Door Opened")
		time.sleep(1)
	else:
		print("Door Closed")
		espeak.set_voice("ru")
		espeak.synth("Door Closed")
		time.sleep(1)
	
	return door_state
 	
# Checks whether the natural language input is valid or not.
def isValid(text):
	stemmed = [porter2.stem(i) for i in text]

	if (len(text) == 1) or ('open' in stemmed and 'close' in stemmed):
		print("Invalid command.")
		espeak.set_voice("ru")
		espeak.synth("Invalid Command")
		time.sleep(1)
		return False

	return True
