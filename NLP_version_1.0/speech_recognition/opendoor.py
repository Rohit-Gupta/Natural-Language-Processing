from changeDoorState import change_state
from espeak import espeak
import time

door_state = False #door is closed
# True => door is opened
# False => door is closed
f=open("NLP.txt","r")
while True: 

	if door_state == True:
		print("Door state: Opened")
		espeak.set_voice("ru")
		espeak.synth("Door state: Opened")
		time.sleep(1)		
	else:
		print("Door state: Closed")
		espeak.set_voice("ru")
		espeak.synth("Door state: Closed")
		time.sleep(1)
	
	cmd=f.readline(10)
	command = cmd.split()
	nlp=''.join(command)
	print(command)
	print(nlp)
	nlp = nlp.lower()
	if len(command) == 0:
		continue
	door_state = change_state(door_state, nlp)
